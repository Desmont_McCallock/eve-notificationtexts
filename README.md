# List of EVE Online Notifications Data and Notification Texts
This repo attempts to compile and keep up to date a list of EVE Online notifications data and their texts.

## *How to contribute*
You will find the compiled list in the [wiki](https://bitbucket.org/Desmont_McCallock/eve-notificationtexts/wiki/Home).
In order to add items to the list just edit the page, add your entry, preview your changes and save them.

The provided data ***does not*** need to be from the **Tranquility** (TQ) server, they can likely be from the **Sinqularity** (SiSi) server, as we understand that there are sensitive data, mostly corporation and alliance ones, that you don't wish to expose.

## *Where to find the needed data*
The data that we need are provided from CCP's XML API and the EVE client it self.

1. **XML API**

    The API endpoints that provide the needed data are:

    * Notifications 
        * *TQ API URL*

            https://api.eveonline.com/char/Notifications.xml.aspx?keyID={YourAPIKeyID}&vCode={YourAPIVerificationCode}&characterID={YourCharacterID}

        From here you will need the *notificationID* to use in the NotificationTexts endpoint and the *typeID* for the list entry.

    * NotificationTexts
        * *TQ API URL*

            https://api.eveonline.com/char/NotificationTexts.xml.aspx?keyID={YourAPIKeyID}&vCode={YourAPIVerificationCode}&characterID={YourCharacterID}&ids={NotificationID}

        From here you will need the data that are inside the *CDATA* area.

    In order to get the needed data from SiSi just replace the host (*api.eveonline.com*) from the above endpoints with *api.testeveonline.com*.

    *You can use your browser of choise to access the data from the above endpoints or any other tool (EVEMon's API Tester, EVEHQ, etc).*

2. **EVE client**

    Open the client and select *Mail* from the navigation taskbar. Now select the *Communications* tab. You will find there all your notifications grouped by gategories.

## *Compiling the list entry*
Now that you have all the needed data lets see how to create the entry.

An entry ***must*** consist from the following parts:

1. TypeID *(this is the **typeID** from the Notification endpoint)*
2. Subject *(the subject is shown in the EVE client as the **subject** of the notification)*
3. Text *(the text is shown in the EVE client)*

Example:

| TypeID | Name | Subject | Text | CDATA (Optional) |
| :- | :- | :- | :- | :- |
| 55 | Insurance Issued | Insurance Contract Issued | Dear valued customer,\n\n\n\nCongratulations on the insurance on your ship. A very wise choice indeed.\n\nThis letter is to confirm that we have issued an insurance contract for your ship, {shipName} ({typeID}) at a level of {level}%.\n\nThis contract will expire at {endDate}, after {numWeeks} weeks.\n\n\n\nBest,\n\nThe Secure Commerce Commission,\n\nReference ID: {itemID} | endDate: 130920666989840146, itemID: 1008438062401, level: 50.0, numWeeks: 12, shipName: '1', startDate: 130848090989840146, typeID: 582 |

As you may notice in the example, the text of the entry is not exactly as it's shown in the EVE client but a combination of the *CDATA* returned in the NotificationTexts endpoint and the text shown in the EVE client.

Each end line and empty line in the text is peplaced with a `\n`.
Also the text sections that correspond to the data in the *CDATA* section of the NotificationTexts is replaced accordingly in our text.

**Is this too much work to do? Fear Not**. If providing the compiled text is too much for you, then you can just copy-paste the text from the EVE client as it is (you will only have to add the `\n` for each end line and empty line) and provide also the CDATA for that text and we will replace the appropriate sections for you.

**Don't have access to edit the wiki but still want to help? Great**. Copy-paste the text from the EVE client as it is, get also the *CDATA* from the API and [EVEMail](https://gate.eveonline.com/Mail/Compose/Desmont%20McCallock) me them.

**BE RESPONSIBLE**. If you notice any inconsistency in the list such as wrong TypeID referencing, wrong Name description or capitalization of the Name, missing lines `\n` from the text, wrong CDATA in the entry, please be kind and correct them.

***We would much appreciate if you refrain from vandalizing the work and effort put into creating this. Thank you.***